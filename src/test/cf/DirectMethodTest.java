package test.cf;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.commons.math3.fraction.BigFraction;
import org.cf.ComputationProblemException;
import org.cf.DirectMethod;
import org.cf.EnumBigIntArrayArraList;
import org.cf.ErrorCheckingException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.math.CFFraction;
import org.math.Poly;

public class DirectMethodTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testNewApproximants() {
        BigInteger[] approximants = new BigInteger[] { BigInteger.ONE,
                BigInteger.ONE, BigInteger.ONE, BigInteger.ONE };
        BigInteger[] result = new BigInteger[] { BigInteger.valueOf(2),
                BigInteger.valueOf(2), BigInteger.ONE, BigInteger.ONE };

        assertArrayEquals(result,
                DirectMethod.newApproximants(approximants, BigInteger.ONE));
    }

    @Test
    public void testApproximants() {
        BigInteger[] approximants = new BigInteger[] { BigInteger.valueOf(3),
                BigInteger.valueOf(2), BigInteger.ONE, BigInteger.ONE };
        BigInteger[] result = new BigInteger[] { BigInteger.valueOf(3363),
                BigInteger.valueOf(2378), BigInteger.valueOf(1393),
                BigInteger.valueOf(985) };
        ArrayList<BigInteger> a = new ArrayList<BigInteger>();
        for (int i = 0; i < 8; i++) {
            a.add(BigInteger.valueOf(2));
        }
        approximants = DirectMethod.approximants(a, approximants);
        assertArrayEquals(result, approximants);
    }

    @Test
    public void testFloor() {
        assertEquals(BigInteger.ONE, DirectMethod.floor(new CFFraction(
                BigInteger.valueOf(3), BigInteger.valueOf(2))));
    }

    @Test
    public void testCheckPnQn() {
        BigInteger[] approximants = new BigInteger[] { BigInteger.ONE,
                BigInteger.ZERO, BigInteger.ZERO, BigInteger.ONE };
        assertTrue(DirectMethod.checkPnQn(approximants, BigInteger.valueOf(1)));

        approximants = new BigInteger[] { BigInteger.ONE, BigInteger.ONE,
                BigInteger.ZERO, BigInteger.ONE };
        assertTrue(DirectMethod.checkPnQn(approximants, BigInteger.valueOf(1)));

        approximants = new BigInteger[] { BigInteger.valueOf(3),
                BigInteger.valueOf(2), BigInteger.ONE, BigInteger.ONE };
        assertTrue(DirectMethod.checkPnQn(approximants, BigInteger.valueOf(1)));

        approximants = new BigInteger[] { BigInteger.valueOf(3363),
                BigInteger.valueOf(2378), BigInteger.valueOf(1393),
                BigInteger.valueOf(985) };
        assertTrue(DirectMethod.checkPnQn(approximants, BigInteger.valueOf(9)));
    }

    @Test
    public void testCalcAlpha() {
        Poly p;
        Poly d;
        BigInteger[] approximants;
        BigFraction expected = new BigFraction(0, 1);
        List<BigInteger> cs = Arrays
                .asList(new BigInteger[] { BigInteger.ONE });
        p = new Poly(cs);
        d = p;
        approximants = new BigInteger[] { BigInteger.ONE, BigInteger.ONE,
                BigInteger.ONE, BigInteger.ONE };
        assertEquals(expected, DirectMethod.calcAlpha(p, d, approximants));

        cs = Arrays.asList(new BigInteger[] { BigInteger.valueOf(-2),
                BigInteger.ZERO, BigInteger.ONE });
        p = new Poly(cs);
        d = p.deriv();
        approximants = new BigInteger[] { BigInteger.valueOf(3),
                BigInteger.valueOf(2), BigInteger.ONE, BigInteger.ONE };
        expected = new BigFraction(5, 2);
        assertEquals(expected, DirectMethod.calcAlpha(p, d, approximants));

        cs = Arrays.asList(new BigInteger[] { BigInteger.valueOf(-2),
                BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO,
                BigInteger.ZERO, BigInteger.ONE });
        p = new Poly(cs);
        d = p.deriv();
        approximants = new BigInteger[] {
                new BigInteger(
                        "68013361521725975191865719579963507845878059334204242827418731756963311717496701266565281165151349458662923725286742609271987657794174371840277517888295051963206480303483634416202620112354719191218856154851548676944828527082829766810146835948180585897187628033555575673728278473154661937715134660263792700474"),
                new BigInteger(
                        "59209070184401482565387751361785840666158307348734621954002605714714858798462374999889214404882353068659909988423485819027894021509840066923538305228058308894904875575588477217909128851673515888964547983467567651587515534021624742882374130421623375570602712321687697753944258470971983669865825249158566882599"),
                new BigInteger(
                        "44672231430732491393053439868879202517935980251460031147663309656600557945033155962850815138690371401504746087397739775415215289182102023153794164480040968536707970845460607542819445768999405465672886123614231305606133878734897332283349650871218610992735386517129742056257143556629727572470250508935628440069"),
                new BigInteger(
                        "38889436235718991960337209277957476079310645463160604652778957909383469111734316738523341719136975648329981429168132648907504464727143226161427234372490960094199907157693763308349112271060261194154423090289029987730341872419009911462314391033385582052762043283586692080863423878127757969540752058807227894218") };

    }

    @Test
    public void testQuadraticPartialQuotient() throws Exception {
        ArrayList<BigInteger> cs = new ArrayList<BigInteger>(
                Arrays.asList(new BigInteger[] { BigInteger.valueOf(-2),
                        BigInteger.ZERO, BigInteger.ONE }));
        Poly p = new Poly(cs);
        BigInteger N = BigInteger.valueOf(120);
        BigInteger b = BigInteger.valueOf(100);

        ArrayList<BigInteger> result = new ArrayList<BigInteger>();
        result.add(BigInteger.ONE);
        for (int i = 0; i < 120 - 1; i++) {
            result.add(BigInteger.valueOf(2));
        }

        try {
            ArrayList<BigInteger> check = DirectMethod.partialQuotient(p, N, b,
                    null);
            assertEquals(result.size(), check.size());
            BigInteger[] actual = new BigInteger[check.size()];
            actual = check.toArray(actual);

            BigInteger[] expected = new BigInteger[result.size()];

            expected = result.toArray(expected);
            assertArrayEquals(expected, actual);
        } catch (InterruptedException | ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }
    }

    @Test
    public void testCubicPartialQuotient() throws Exception {
        ArrayList<BigInteger> cs = new ArrayList<BigInteger>(
                Arrays.asList(new BigInteger[] { BigInteger.valueOf(-2),
                        BigInteger.ZERO, BigInteger.ONE }));
        Poly p = new Poly(cs);
        BigInteger N = BigInteger.valueOf(1000);
        BigInteger b = BigInteger.valueOf(100);

        cs = new ArrayList<BigInteger>(Arrays.asList(new BigInteger[] {
                BigInteger.valueOf(-2), BigInteger.ZERO, BigInteger.ZERO,
                BigInteger.ONE }));
        p = new Poly(cs);

        int[] intArray = { 1, 3, 1, 5, 1, 1, 4, 1, 1, 8, 1, 14, 1, 10, 2, 1, 4,
                12, 2, 3, 2, 1, 3, 4, 1, 1, 2, 14, 3, 12, 1, 15, 3, 1, 4, 534,
                1, 1, 5, 1, 1, 121, 1, 2, 2, 4, 10, 3, 2, 2, 41, 1, 1, 1, 3, 7,
                2, 2, 9, 4, 1, 3, 7, 6, 1, 1, 2, 2, 9, 3, 1, 1, 69, 4, 4, 5,
                12, 1, 1, 5, 15, 1, 4, 1, 1, 1, 1, 1, 89, 1, 22, 186, 6, 2, 3,
                1, 3, 2, 1, 1, 5, 1, 3, 1, 8, 9, 1, 26, 1, 7, 1, 18, 6, 1, 372,
                3, 13, 1, 1, 14, 2, 2, 2, 1, 1, 4, 3, 2, 2, 1, 1, 9, 1, 6, 1,
                38, 1, 2, 25, 1, 4, 2, 44, 1, 22, 2, 12, 11, 1, 1, 49, 2, 6, 8,
                2, 3, 2, 1, 3, 5, 1, 1, 1, 3, 1, 2, 1, 2, 4, 1, 1, 3, 2, 1, 9,
                4, 1, 4, 1, 2, 1, 27, 1, 1, 5, 5, 1, 3, 2, 1, 2, 2, 3, 1, 4, 2,
                2, 8, 4, 1, 6, 1, 1, 1, 36, 9, 13, 9, 3, 6, 2, 5, 1, 1, 1, 2,
                10, 21, 1, 1, 1, 2, 1, 2, 6, 2, 1, 6, 19, 1, 1, 18, 1, 2, 1, 1,
                1, 27, 1, 1, 10, 3, 11, 38, 7, 1, 1, 1, 3, 1, 8, 1, 5, 1, 5, 4,
                4, 4, 7, 2, 1, 21, 1, 1, 5, 10, 3, 1, 72, 6, 9, 1, 3, 3, 2, 1,
                4, 2, 1, 1, 1, 1, 2, 1, 7, 8, 1, 2, 1, 8, 1, 8, 3, 1, 1, 3, 2,
                1, 8, 1, 1, 1, 1, 1, 6, 1, 4, 3, 4, 1, 1, 1, 4, 30, 39, 2, 1,
                3, 8, 1, 1, 2, 1, 3, 1, 9, 1, 4, 1, 2, 2, 1, 6, 2, 1, 1, 3, 1,
                4, 1, 2, 1, 1, 5, 1, 2, 10, 1, 5, 4, 1, 1, 4, 1, 2, 1, 1, 2,
                12, 2, 1, 8, 3, 2, 6, 1, 3, 10, 1, 2, 20, 1, 6, 1, 2, 186, 2,
                2, 1, 2, 47, 1, 19, 2, 2, 1, 1, 1, 2, 1, 1, 3, 2, 8, 1, 18, 3,
                5, 39, 1, 2, 1, 1, 1, 1, 4, 1, 5, 2, 6, 3, 1, 1, 1, 4, 2, 1, 6,
                1, 1, 220, 1, 3, 1, 3, 1, 4, 5, 1, 2, 1, 13, 2, 2, 2, 1, 1, 1,
                1, 7, 2, 1, 7, 1, 3, 1, 1, 11, 1, 2, 2, 4, 2, 33, 3, 1, 1, 2,
                6, 3, 1, 1, 3, 6, 8, 3, 4, 84, 1, 1, 2, 1, 10, 2, 2, 20, 1, 3,
                1, 7, 13, 14, 1, 29, 1, 1, 5, 1, 7, 1, 1, 2, 1, 56, 1, 3, 2, 1,
                13, 2, 1, 2, 2, 2, 1, 1, 1, 1, 1, 1, 255, 2, 4, 5, 1, 1, 1, 3,
                1, 3, 3, 1, 6, 1, 1, 6, 1, 71, 1, 9, 1, 2, 1, 11, 5, 1, 25, 1,
                6, 67, 2, 9, 6, 1, 5, 2, 15, 1, 2, 48, 2, 7, 1, 3, 1, 4, 21, 1,
                1, 2, 1, 27, 3, 26, 2, 1, 1, 2, 5, 7, 3, 7451, 2, 29, 4, 3, 8,
                17, 3, 8, 2, 3, 1, 1, 1, 5, 113, 1, 3, 4, 1, 4, 1, 1, 13, 1,
                34, 1, 2, 7, 1, 3, 3, 7, 1, 3, 1, 1, 4, 2, 69, 1, 3, 12, 34, 1,
                2, 151, 1, 4941, 4, 1, 1, 12, 3, 4, 2, 3, 1, 1, 1, 1, 1, 2, 1,
                1, 6, 16, 1, 2, 27, 2, 13, 4, 1, 1, 1, 3, 11, 1, 1, 3, 1, 53,
                2, 15, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 3, 3, 1, 9, 1, 1, 10, 3,
                1, 1, 2, 1, 2, 2, 1, 10, 9, 1, 2, 5, 1, 2, 2, 1, 1, 2, 4, 7, 1,
                5, 1, 1, 1, 1, 4, 2, 25, 16, 5, 4, 1, 3, 2, 3, 13, 1, 49, 6, 2,
                5, 1, 1, 2, 7, 3, 2, 1, 1, 1, 4, 1, 1, 1, 5, 1, 2, 1, 2, 1, 1,
                1, 1, 2, 2, 4, 1, 2, 1, 10, 5, 4, 8, 10, 2, 4, 1, 1, 1, 4, 1,
                41, 1, 3, 1, 56, 3, 1, 1, 3, 1, 3, 1, 5, 6, 6, 3, 1, 2, 1, 1,
                1, 12, 1, 10, 2, 1, 1, 1, 1, 50, 5, 1, 2, 6, 5, 1, 2, 5, 6, 5,
                2, 77, 1, 4, 2, 1, 1, 1, 1, 1, 4, 2, 1, 2, 1, 1, 1, 1, 1, 6, 2,
                1, 1, 7, 1, 5, 1, 1, 1, 1, 2, 2, 1, 1, 5, 2, 1, 5, 1, 1, 1, 4,
                1, 2, 17, 1, 20, 7, 4, 2, 1, 1, 1, 2, 1, 4, 7, 3, 4, 3, 3, 5,
                31, 1, 1, 2, 2, 6, 1, 1, 1, 1, 1, 1, 1, 1, 6, 1, 6, 1, 1, 23,
                20, 1, 22, 16, 4, 2, 1, 3, 2, 1, 1, 2, 5, 5, 1, 1, 15, 3, 1, 1,
                2, 1, 1, 1, 4, 2, 1, 2, 23, 6, 10, 3, 2, 3, 6, 2, 1, 1, 1, 1,
                1, 1, 4, 3, 2, 1, 2, 1, 4, 10, 7, 1, 1, 1, 1, 3, 3, 2, 108, 1,
                1, 11, 2, 6, 1, 4, 1, 2, 2, 9, 3, 1, 1, 3, 22, 4, 1, 93, 1, 3,
                1, 4, 2, 1, 2, 3, 2, 1, 2, 11, 1, 1, 3, 1, 2, 1, 28, 23, 4, 11,
                1, 9, 1, 4, 3, 1, 6, 1, 2, 1, 12, 2, 6, 19, 1, 4, 4, 2, 1, 1,
                1, 1 };
        ArrayList<BigInteger> result = new ArrayList<BigInteger>();
        for (int i : intArray) {
            result.add(BigInteger.valueOf(i));
        }
        N = BigInteger.valueOf(1000);
        try {
            ArrayList<BigInteger> check = DirectMethod.partialQuotient(p, N, b,
                    null);
            assertEquals(result.size(), check.size());
            BigInteger[] actual = new BigInteger[check.size()];
            actual = check.toArray(actual);

            BigInteger[] expected = new BigInteger[result.size()];

            expected = result.toArray(expected);
            assertArrayEquals(expected, actual);
        } catch (InterruptedException | ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }
    }

    @Test
    public void testNegativePartialQuotientException() throws Exception {
        thrown.expect(ComputationProblemException.class);
        // you can test the exception message like
        ArrayList<BigInteger> cs = new ArrayList<BigInteger>(
                Arrays.asList(new BigInteger[] { BigInteger.valueOf(-2),
                        BigInteger.ZERO, BigInteger.ONE }));
        Poly p = new Poly(cs);
        BigInteger N = BigInteger.valueOf(10000);
        BigInteger b = BigInteger.valueOf(130);

        thrown.expectMessage("b is " + b);
        cs = new ArrayList<BigInteger>(Arrays.asList(new BigInteger[] {
                BigInteger.valueOf(-2), BigInteger.ZERO, BigInteger.ZERO,
                BigInteger.ZERO, BigInteger.ZERO, BigInteger.ONE }));
        p = new Poly(cs);

        try {
            ArrayList<BigInteger> check = DirectMethod.partialQuotient(p, N, b,
                    null);
            boolean checkNegative = false;
            for (BigInteger a : check) {
                if (a.compareTo(BigInteger.ZERO) <= 0) {
                    checkNegative = true;
                }
            }
            assertFalse(checkNegative);
        } catch (InterruptedException | ExecutionException | IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testExtractCFFromSolver() throws ComputationProblemException,
            ErrorCheckingException {
        ArrayList<BigInteger> cs = new ArrayList<BigInteger>(
                Arrays.asList(new BigInteger[] { BigInteger.valueOf(-51),
                        BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO,
                        BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO,
                        BigInteger.ZERO, BigInteger.ZERO, BigInteger.ONE }));
        Poly p = new Poly(cs);
        BigInteger N = BigInteger.valueOf(100);
        BigInteger b = BigInteger.valueOf(10000);

        BigInteger n = BigInteger.ONE.negate();

        BigInteger[] currentApproximants = DirectMethod.initialValues();

        EnumBigIntArrayArraList ab = DirectMethod.extractCFFromSolver(p, N, b,
                n, currentApproximants);
        int min = Math.min(ab.getSubContinuedFraction().size(), 100);
        assertEquals(min, 100);
    }

    @Test
    public void testExtractCFFromPoly() throws ComputationProblemException,
            InterruptedException, ExecutionException, ErrorCheckingException {
        ArrayList<BigInteger> cs = new ArrayList<BigInteger>(
                Arrays.asList(new BigInteger[] { BigInteger.valueOf(-51),
                        BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO,
                        BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO,
                        BigInteger.ZERO, BigInteger.ZERO, BigInteger.ONE }));
        Poly p = new Poly(cs);
        BigInteger N = BigInteger.valueOf(100);
        BigInteger b = BigInteger.valueOf(100);

        BigInteger[] currentApproximants = DirectMethod.initialValues();

        EnumBigIntArrayArraList ab = DirectMethod.extractCFFromPolyMethod(p, N,
                b, currentApproximants);

        assertEquals(ab.getSubContinuedFraction().size(), 100);
    }

    @Test
    public void testSpeed() throws Exception {
        ArrayList<BigInteger> cs = new ArrayList<BigInteger>(
                Arrays.asList(new BigInteger[] { BigInteger.valueOf(-50),
                        BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO,
                        BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO,
                        BigInteger.ZERO, BigInteger.ZERO, BigInteger.ONE }));
        Poly p = new Poly(cs);
        BigInteger N = BigInteger.valueOf(100);
        BigInteger b = BigInteger.valueOf(100);

        DirectMethod.partialQuotient(p, N, b, null);

    }
}
