package test.cf;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.cf.Computation;
import org.junit.Test;
import org.math.Maths;
import org.math.Poly;

public class ComputationTest {

    @Test
    public void testFactorial() {
        assertEquals("0! is 1", 1, Maths.factorial(0));
        assertEquals("1! is 1", 1, Maths.factorial(1));
        assertEquals("2! is 2", 2, Maths.factorial(2));
        assertEquals("6! is 6*5*4*3*2", 6 * 5 * 4 * 3 * 2, Maths.factorial(6));
    }

    @Test
    public void testCombinations() {
        assertEquals("1C1 is 1", 1, Maths.combinations(1, 1));
        assertEquals("5C5 is 1", 1, Maths.combinations(5, 5));
        assertEquals("2C1 is 2", 2, Maths.combinations(2, 1));

        assertEquals("0C0 is 1", 1, Maths.combinations(0, 0));
    }

    @Test
    public void testGetContinuedFracOpt() throws InterruptedException,
            ExecutionException {
        Poly p;
        int n;
        // text x^2 -2
        BigInteger[] array = new BigInteger[] { BigInteger.valueOf(-2),
                BigInteger.ZERO, BigInteger.ONE };
        List<BigInteger> coeffs = Arrays.asList(array);
        p = new Poly(coeffs);

        n = 100;
        ArrayList<BigInteger> result = new ArrayList<BigInteger>();
        result.add(BigInteger.ONE);
        for (int i = 0; i < n - 1; i++) {
            result.add(BigInteger.valueOf(2));
        }
        assertEquals(result, Computation.getContinuedFracOpt(p, n));

        // text x^3 - 2
        array = new BigInteger[] { BigInteger.valueOf(-2), BigInteger.ZERO,
                BigInteger.ZERO, BigInteger.ONE };
        coeffs = Arrays.asList(array);
        p = new Poly(coeffs);

        n = 100;
        result = new ArrayList<BigInteger>();
        int[] intArray = { 1, 3, 1, 5, 1, 1, 4, 1, 1, 8, 1, 14, 1, 10, 2, 1, 4,
                12, 2, 3, 2, 1, 3, 4, 1, 1, 2, 14, 3, 12, 1, 15, 3, 1, 4, 534,
                1, 1, 5, 1, 1, 121, 1, 2, 2, 4, 10, 3, 2, 2, 41, 1, 1, 1, 3, 7,
                2, 2, 9, 4, 1, 3, 7, 6, 1, 1, 2, 2, 9, 3, 1, 1, 69, 4, 4, 5,
                12, 1, 1, 5, 15, 1, 4, 1, 1, 1, 1, 1, 89, 1, 22, 186, 6, 2, 3,
                1, 3, 2, 1, 1 };
        for (int i : intArray) {
            result.add(BigInteger.valueOf(i));
        }
        assertEquals(result, Computation.getContinuedFracOpt(p, n));

    }

    @Test
    public void testContinuedFraction() throws Exception {
        int n;
        // text x^2 -2
        BigInteger[] array = new BigInteger[] { BigInteger.valueOf(-2),
                BigInteger.ZERO, BigInteger.ONE };
        List<BigInteger> coeffs = Arrays.asList(array);
        n = 100;
        ArrayList<BigInteger> result = new ArrayList<BigInteger>();
        result.add(BigInteger.ONE);
        for (int i = 0; i < n - 1; i++) {
            result.add(BigInteger.valueOf(2));
        }
        assertEquals(result,
                Computation.continuedFraction(BigInteger.valueOf(n), coeffs));

        // text x^3 - 2
        array = new BigInteger[] { BigInteger.valueOf(-2), BigInteger.ZERO,
                BigInteger.ZERO, BigInteger.ONE };
        coeffs = Arrays.asList(array);
        n = 100;
        result = new ArrayList<BigInteger>();
        int[] intArray = { 1, 3, 1, 5, 1, 1, 4, 1, 1, 8, 1, 14, 1, 10, 2, 1, 4,
                12, 2, 3, 2, 1, 3, 4, 1, 1, 2, 14, 3, 12, 1, 15, 3, 1, 4, 534,
                1, 1, 5, 1, 1, 121, 1, 2, 2, 4, 10, 3, 2, 2, 41, 1, 1, 1, 3, 7,
                2, 2, 9, 4, 1, 3, 7, 6, 1, 1, 2, 2, 9, 3, 1, 1, 69, 4, 4, 5,
                12, 1, 1, 5, 15, 1, 4, 1, 1, 1, 1, 1, 89, 1, 22, 186, 6, 2, 3,
                1, 3, 2, 1, 1 };
        for (int i : intArray) {
            result.add(BigInteger.valueOf(i));
        }
        assertEquals(result,
                Computation.continuedFraction(BigInteger.valueOf(n), coeffs));
    }

    @Test
    public void testSpeed() throws Exception {
        ArrayList<BigInteger> cs = new ArrayList<BigInteger>(
                Arrays.asList(new BigInteger[] { BigInteger.valueOf(-50),
                        BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO,
                        BigInteger.ZERO, BigInteger.ZERO, BigInteger.ZERO,
                        BigInteger.ZERO, BigInteger.ZERO, BigInteger.ONE }));
        Poly p = new Poly(cs);
        int N = 10000;

        Computation.getContinuedFracOpt(p, N);

    }

    @Test
    public void testGetNextContinuedFrac() {
        // test poly x - 1
        ArrayList<BigInteger> l = new ArrayList<BigInteger>();
        BigInteger a;

        l.add(BigInteger.valueOf(-2));
        l.add(BigInteger.valueOf(1));
        a = Computation.getNextContinuedFrac(new Poly(l));
        assertEquals("Poly p(x) = x-2 has root 2", BigInteger.valueOf(1), a);

        // test poly x^2 - 2
        l.clear();
        l.add(BigInteger.valueOf(-2));
        l.add(BigInteger.valueOf(0));
        l.add(BigInteger.valueOf(1));
        a = Computation.getNextContinuedFrac(new Poly(l));
        assertEquals("Poly p(x) = x^2 - 2 has root 1....", BigInteger.ONE, a);

        // test poly x^2 - 5
        l.clear();
        l.add(BigInteger.valueOf(-5));
        l.add(BigInteger.valueOf(0));
        l.add(BigInteger.valueOf(1));
        a = Computation.getNextContinuedFrac(new Poly(l));
        assertEquals("Poly p(x) = x^2 - 5 has root 2....",
                BigInteger.valueOf(2), a);

        // test poly x^2 - 5
        l.clear();
        l.add(BigInteger.valueOf(-500));
        l.add(BigInteger.valueOf(0));
        l.add(BigInteger.valueOf(1));
        a = Computation.getNextContinuedFrac(new Poly(l));
        assertEquals("Poly p(x) = x^2 - 500 has root 22....",
                BigInteger.valueOf(22), a);

    }

    @Test
    public void testGetNextContinuedFracOpt() {
        // test poly x - 1
        ArrayList<BigInteger> l = new ArrayList<BigInteger>();
        BigInteger a;
        try {
            for (int j = 2; j < 5; j++) {
                l.clear();
                l.add(BigInteger.valueOf(-2));
                l.add(BigInteger.valueOf(1));
                a = Computation.getNextContinuedFracOpt(new Poly(l), j);
                assertEquals("Poly p(x) = x-2 has root 2", BigInteger.ONE, a);

                l.clear();
                l.add(BigInteger.valueOf(-5));
                l.add(BigInteger.valueOf(1));
                a = Computation.getNextContinuedFracOpt(new Poly(l), j);
                assertEquals("Poly p(x) = x-5 has root 5",
                        BigInteger.valueOf(4), a);

                // test poly x^2 - 2
                l.clear();
                l.add(BigInteger.valueOf(-2));
                l.add(BigInteger.valueOf(0));
                l.add(BigInteger.valueOf(1));
                a = Computation.getNextContinuedFracOpt(new Poly(l), j);
                assertEquals("Poly p(x) = x^2 - 2 has root 1....",
                        BigInteger.ONE, a);

                // test poly x^2 - 5
                l.clear();
                l.add(BigInteger.valueOf(-5));
                l.add(BigInteger.valueOf(0));
                l.add(BigInteger.valueOf(1));
                a = Computation.getNextContinuedFracOpt(new Poly(l), j);
                assertEquals("Poly p(x) = x^2 - 5 has root 2....",
                        BigInteger.valueOf(2), a);

                // test poly x^2 - 5
                l.add(BigInteger.valueOf(-500));
                l.add(BigInteger.valueOf(0));
                l.add(BigInteger.valueOf(1));
                a = Computation.getNextContinuedFracOpt(new Poly(l), j);
                assertEquals("Poly p(x) = x^2 - 500 has root 22....",
                        BigInteger.valueOf(22), a);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testPartialPoly() throws Exception {
        ArrayList<BigInteger> l = new ArrayList<BigInteger>();
        // test poly x^2 - 2
        l.add(BigInteger.valueOf(-2));
        l.add(BigInteger.valueOf(0));
        l.add(BigInteger.valueOf(1));

        Poly prev = new Poly(l);
        BigInteger a = BigInteger.ONE;
        ArrayList<BigInteger> p = null;
        p = Computation.partialPoly(prev, a);

        l.clear();
        l.add(BigInteger.valueOf(-1));
        l.add(BigInteger.valueOf(2));
        l.add(BigInteger.valueOf(1));
        assertEquals(
                "Poly p(x) = x^2 - 2 next partial poly should be -1+2x+x^2", l,
                p);

        l.clear();
        l.add(BigInteger.valueOf(-1));
        l.add(BigInteger.valueOf(-2));
        l.add(BigInteger.valueOf(1));

        prev = new Poly(l);
        a = BigInteger.valueOf(2);
        p = Computation.partialPoly(prev, a);

        l.clear();
        l.add(BigInteger.valueOf(-1));
        l.add(BigInteger.valueOf(2));
        l.add(BigInteger.valueOf(1));
        assertEquals(
                "Poly p(x) = x^2 - 2x - 1 next partial poly should be -1+2x+x^2",
                l, p);
    }

    @Test
    public void testNextPoly() throws Exception {
        ArrayList<BigInteger> l = new ArrayList<BigInteger>();
        // test poly x^2 - 2
        l.add(BigInteger.valueOf(-2));
        l.add(BigInteger.valueOf(0));
        l.add(BigInteger.valueOf(1));

        Poly prev = new Poly(l);
        BigInteger a = Computation.getNextContinuedFrac(prev);
        Poly p = null;
        p = Computation.nextPoly(prev, a);

        l.clear();
        l.add(BigInteger.valueOf(-1));
        l.add(BigInteger.valueOf(-2));
        l.add(BigInteger.valueOf(1));
        assertEquals("Poly p(x) = x^2 - 2 next poly should be -1-2x+x^2", l,
                p.getCoeffs());
    }

    @Test
    public void testNextTerm() {
        // Set up
        ArrayList<BigInteger> l = new ArrayList<BigInteger>();
        l.add(BigInteger.valueOf(-2));
        l.add(BigInteger.valueOf(0));
        l.add(BigInteger.valueOf(1));
        try {
            assertEquals("Degree 2, count 0, poly x^2 -2, a = 1",
                    BigInteger.valueOf(-1),
                    Computation.nextTerm(BigInteger.ONE, l, 2, 0));
            assertEquals("Degree 2, count 1, poly x^2 -2, a = 1",
                    BigInteger.valueOf(2),
                    Computation.nextTerm(BigInteger.ONE, l, 2, 1));
            assertEquals("Degree 2, count 2, poly x^2 -2, a = 1",
                    BigInteger.valueOf(1),
                    Computation.nextTerm(BigInteger.ONE, l, 2, 2));
        } catch (Exception e) {
            e.printStackTrace();
        }

        l.clear();
        l.add(BigInteger.valueOf(-1));
        l.add(BigInteger.valueOf(-2));
        l.add(BigInteger.valueOf(1));
        try {
            assertEquals("Degree 2, count 0, poly x^2 -2x -1, a = 2",
                    BigInteger.valueOf(-1),
                    Computation.nextTerm(BigInteger.valueOf(2), l, 2, 0));
            assertEquals("Degree 2, count 1, poly x^2 -2x -1, a = 2",
                    BigInteger.valueOf(2),
                    Computation.nextTerm(BigInteger.valueOf(2), l, 2, 1));
            assertEquals("Degree 2, count 2, poly x^2 -2x -1, a = 2",
                    BigInteger.valueOf(1),
                    Computation.nextTerm(BigInteger.valueOf(2), l, 2, 2));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
