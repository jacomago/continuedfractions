package org.cf;

public class ComputationProblemException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public ComputationProblemException(String message) {
        super(message);
    }
}
