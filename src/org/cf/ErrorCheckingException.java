package org.cf;

public class ErrorCheckingException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    ErrorCheckingException(String message) {
        super(message);
    }
}
