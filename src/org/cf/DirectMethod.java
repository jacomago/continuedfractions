package org.cf;

import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import org.apache.commons.math3.fraction.BigFraction;
import org.math.CFFraction;
import org.math.Maths;
import org.math.Poly;

public class DirectMethod {
    /**
     * Finds the bigfraction from bigdecimal
     * 
     * @param bd
     * @return bigfraction with bd value
     */
    public static BigFraction bigDecimalToBigFraction(BigDecimal bd) {
        int scale = bd.scale();

        // If scale >= 0 then the value is bd.unscaledValue() / 10^scale
        if (scale >= 0)
            return new BigFraction(bd.unscaledValue(),
                    BigInteger.TEN.pow(scale));
        // If scale < 0 then the value is bd.unscaledValue() * 10^-scale
        return new BigFraction(bd.unscaledValue().multiply(
                BigInteger.TEN.pow(-scale)));
    }

    /**
     * Calculates the continued fraction of the root of p for n values and
     * prints with w
     * 
     * @param p
     *            the polynomial
     * @param values
     *            how many to calculate
     * @param b
     *            error check
     * @param w
     *            writer
     * @return partial quotient
     * @throws InterruptedException
     * @throws ExecutionException
     * @throws IOException
     * @throws ErrorCheckingException
     */
    public static ArrayList<BigInteger> partialQuotient(Poly p,
            BigInteger values, BigInteger b, BufferedWriter w)
            throws InterruptedException, ExecutionException, IOException,
            ErrorCheckingException {
        ArrayList<BigInteger> a = new ArrayList<BigInteger>();

        BigInteger n = BigInteger.ONE.negate();

        BigInteger[] currentApproximants = initialValues();

        EnumBigIntArrayArraList cAscf = extractCFFromPolyMethod(p, values, b,
                currentApproximants);

        a = cAscf.getSubContinuedFraction();
        currentApproximants = cAscf.currentApproximants;
        n = n.add(BigInteger.valueOf(a.size()));
        if (w != null) {
            printLastArray(w, a);
        }

        Poly d = p.deriv();
        while (checkCalcEnough(n, values)) {
            throwPnQn(n, currentApproximants);
            BigFraction alpha = calcAlpha(p, d, currentApproximants);
            BigInteger B = currentApproximants[1].pow(2).divide(b);

            cAscf = subLoop(n, values, currentApproximants, B, alpha, false);
            ArrayList<BigInteger> as = cAscf.getSubContinuedFraction();
            currentApproximants = cAscf.currentApproximants;

            n = n.add(BigInteger.valueOf(as.size()));
            a.addAll(as);

            if (w != null) {
                printLastArray(w, as);
            }

        }
        return a;
    }

    /**
     * Creates the first pnqn
     * 
     * @return the pnqn
     */
    public static BigInteger[] initialValues() {
        BigInteger xn1 = BigInteger.ONE;
        BigInteger yn1 = BigInteger.ZERO;

        BigInteger xn = BigInteger.ZERO;
        BigInteger yn = BigInteger.ONE;

        BigInteger[] currentApproximants = new BigInteger[] { xn1, yn1, xn, yn };
        return currentApproximants;
    }

    /**
     * Calculates initial values from a decimal approximation of the root of the
     * polynomial
     * 
     * @param p
     *            the polynomial
     * @param values
     *            how many to calculate
     * @param b
     *            error check
     * @param n
     *            initial amount
     * @param currentApproximants
     *            initial approximation pn qn
     * @return the approximants and continued fraction
     * @throws ErrorCheckingException
     */
    public static EnumBigIntArrayArraList extractCFFromSolver(Poly p,
            BigInteger values, BigInteger b, BigInteger n,
            BigInteger[] currentApproximants) throws ErrorCheckingException {
        int power = 100;
        int amountDecimal = (int) Math.pow(b.intValue(), power);
        ArrayList<BigDecimal> roots = p.solve(amountDecimal);
        Maths.log("root", roots.get(0));
        BigFraction approx = bigDecimalToBigFraction(roots.get(0));
        Maths.log("approx", approx);

        EnumBigIntArrayArraList cAscf = subLoop(n, values, currentApproximants,
                BigInteger.valueOf(amountDecimal), approx, true);
        return cAscf;
    }

    /**
     * Calculates initial values using the polynomial method
     * 
     * @param p
     *            the polynomial
     * @param values
     *            how many to calculate
     * @param b
     *            error check
     * @param currentApproximants
     *            initial approximation pn qn
     * @return the approximants and continued fraction
     * @throws ErrorCheckingException
     * 
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public static EnumBigIntArrayArraList extractCFFromPolyMethod(Poly p,
            BigInteger values, BigInteger b, BigInteger[] currentApproximants)
            throws ErrorCheckingException, InterruptedException,
            ExecutionException {

        ArrayList<BigInteger> a = Computation.getContinuedFracOpt(p,
                b.intValue());
        currentApproximants = approximants(a, currentApproximants);

        EnumBigIntArrayArraList cAscf = new EnumBigIntArrayArraList(
                currentApproximants, a);
        return cAscf;
    }

    /**
     * Does the basic method for calculating the continued fraction for values
     * amount
     * 
     * @param n
     *            starting point
     * @param values
     *            amount of partial quotients to calculate
     * @param currentApproximants
     *            past pn, qn
     * @param B
     *            error checking constant
     * @param Balpha
     *            approximation of the value
     * @return partial quotients and updated pn qn
     * @throws ErrorCheckingException
     */
    public static EnumBigIntArrayArraList subLoop(BigInteger n,
            BigInteger values, BigInteger[] currentApproximants, BigInteger B,
            BigFraction Balpha, boolean ignore) throws ErrorCheckingException {

        ArrayList<BigInteger> as = new ArrayList<BigInteger>();

        CFFraction alpha = new CFFraction(Balpha);

        while (checkCalcEnough(n, values)
                && checkErrorSmall(B, currentApproximants, ignore)) {
            throwPnQn(n, currentApproximants);

            BigInteger an = floor(alpha);

            throwTooSmallB(an);
            n = n.add(BigInteger.ONE);
            as.add(an);

            currentApproximants = newApproximants(currentApproximants, an);
            alpha = newAlpha(alpha, an);
        }

        return new EnumBigIntArrayArraList(currentApproximants, as);
    }

    /**
     * Error check
     * 
     * @param B
     * @param currentApproximants
     * @return true if B > qn
     */
    private static boolean checkErrorSmall(BigInteger B,
            BigInteger[] currentApproximants, boolean ignore) {
        return ignore || B.compareTo(currentApproximants[1]) > 0;
    }

    /**
     * check if you've calculated enough
     * 
     * @param n
     *            how many calculated
     * @param values
     *            how many to calculate
     * @return true if values > n
     */
    private static boolean checkCalcEnough(BigInteger n, BigInteger values) {
        return !(n.compareTo(values.subtract(BigInteger.ONE)) >= 0);
    }

    /**
     * check the pnqn obey known property
     * 
     * @param n
     *            the current n
     * @param currentApproximants
     *            current approximants
     * @throws ErrorCheckingException
     */
    private static void throwPnQn(BigInteger n, BigInteger[] currentApproximants)
            throws ErrorCheckingException {
        if (!checkPnQn(currentApproximants, n)) {
            throw new ErrorCheckingException("pnqn problem subloop");
        }
    }

    /**
     * Throw exception if bad values are calculated
     * 
     * @param an
     *            input partial quotient
     * @throws ErrorCheckingException
     */
    private static void throwTooSmallB(BigInteger an)
            throws ErrorCheckingException {
        if (an.compareTo(BigInteger.ZERO) <= 0) {
            throw new ErrorCheckingException("To small B");
        }
    }

    /**
     * From approximation get new approximation
     * 
     * @param CFalpha
     *            approximation
     * @param an
     *            partial quotient
     * @return new approximation
     */
    public static CFFraction newAlpha(CFFraction CFalpha, BigInteger an) {
        return new CFFraction(CFalpha.getDenom(), CFalpha.getNum().subtract(
                an.multiply(CFalpha.getDenom())));
    }

    /**
     * Finds the latest two approximants to the root given the continued
     * fraction
     * 
     * @param a
     *            continued fraction
     * 
     * @param previousApproximants
     * 
     * @return new pnqn
     */
    public static BigInteger[] approximants(ArrayList<BigInteger> a,
            BigInteger[] previousApproximants) {
        BigInteger[] newApproximants = new BigInteger[4];
        System.arraycopy(previousApproximants, 0, newApproximants, 0,
                previousApproximants.length);
        for (BigInteger an : a) {
            newApproximants = newApproximants(newApproximants, an);
        }
        return newApproximants;
    }

    public static BigInteger[] newApproximants(BigInteger[] oldApproximants,
            BigInteger an) {
        BigInteger[] newApproximants = new BigInteger[4];
        BigInteger newxn1 = an.multiply(oldApproximants[0]).add(
                oldApproximants[2]);
        BigInteger newyn1 = an.multiply(oldApproximants[1]).add(
                oldApproximants[3]);
        newApproximants[2] = oldApproximants[0];
        newApproximants[3] = oldApproximants[1];
        newApproximants[0] = newxn1;
        newApproximants[1] = newyn1;
        return newApproximants;
    }

    private static void printLastArray(BufferedWriter w, ArrayList<BigInteger> a)
            throws IOException {
        for (BigInteger ai : a) {
            w.write(ai.toString());
            w.newLine();
        }
    }

    public static BigFraction calcAlpha(Poly p, Poly d,
            BigInteger[] approximants) {
        BigFraction tn = new BigFraction(approximants[0], approximants[1]);
        BigInteger yn = approximants[3];
        BigInteger yn1 = approximants[1];
        BigFraction dt = d.result(tn).abs();
        BigFraction pt = p.result(tn).abs();
        BigInteger num = (dt.getNumerator().multiply(pt.getDenominator()))
                .subtract(dt.getDenominator().multiply(pt.getNumerator())
                        .multiply(yn.multiply(yn1)));
        BigInteger denom = yn1.pow(2).multiply(
                dt.getDenominator().multiply(pt.getNumerator()));

        return new BigFraction(num, denom);
    }

    public static BigInteger floor(CFFraction f) {
        BigInteger num = f.getNum();
        BigInteger denom = f.getDenom();
        BigInteger n = BigInteger.ZERO;
        while (num.compareTo(BigInteger.ZERO) > 0) {
            n = n.add(BigInteger.ONE);
            num = num.subtract(denom);
        }
        return n.subtract(BigInteger.ONE);
    }

    public static boolean checkPnQn(BigInteger[] approximants, BigInteger n) {
        BigInteger pn1 = approximants[0];
        BigInteger qn1 = approximants[1];

        BigInteger pn = approximants[2];
        BigInteger qn = approximants[3];

        BigInteger test = pn1.multiply(qn).subtract(pn.multiply(qn1));
        BigInteger mod = n.remainder(BigInteger.valueOf(2)).abs();
        BigInteger comp = new BigInteger("-1");
        if (mod.compareTo(BigInteger.ONE) == 0) {
            comp = BigInteger.ONE;
        }
        if (test.compareTo(comp) == 0) {
            return true;
        } else
            return false;
    }

}
