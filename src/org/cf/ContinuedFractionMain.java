package org.cf;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

import org.apache.commons.math3.fraction.BigFraction;
import org.math.Poly;

public class ContinuedFractionMain {

    /**
     * Prints out the continued fraction of a number, with input polynomial
     * coefficients then number of values to ca
     * 
     * @param args
     */
    public static void main(String[] args) throws Exception {
        String number = args[0];
        String bs = args[1];
        String poly = args[2];

        BigInteger values = new BigInteger(number);
        ArrayList<BigInteger> coeffs = createCoeffs(poly);

        BufferedWriter w = new BufferedWriter(
                new OutputStreamWriter(System.out));
        w.write("The coeffs are " + coeffs);
        w.newLine();
        DirectMethod.partialQuotient(new Poly(coeffs), values, new BigInteger(
                bs), w);
        w.close();
    }

    public static ArrayList<BigInteger> createCoeffs(String s) {
        String[] strCoeffs = s.split(",");
        ArrayList<BigInteger> coeffs = new ArrayList<BigInteger>();
        for (String c : strCoeffs) {
            coeffs.add(new BigInteger(c));
        }
        return coeffs;
    }

    public static void continuedFractionAverages(BigInteger values,
            ArrayList<BigInteger> coeffs, BufferedWriter w, int numProcesses)
            throws Exception {
        Poly first = new Poly(coeffs);
        Poly p = first;

        BigInteger sum = BigInteger.ZERO;
        BigFraction mean = BigFraction.ZERO;
        BigFraction doubleMean = BigFraction.ZERO;

        int scale = 10;
        for (BigInteger i = BigInteger.ONE; i.compareTo(values) <= 0; i = i
                .add(BigInteger.ONE)) {
            BigFraction counter = new BigFraction(i);
            BigInteger a = Computation.getNextContinuedFracOpt(p, numProcesses);
            sum = sum.add(a);
            mean = (new BigFraction(sum)).divide(counter);
            doubleMean = mean.divide(counter);
            w.write(a + "," + sum + ","
                    + mean.bigDecimalValue(scale, BigDecimal.ROUND_DOWN) + ","
                    + doubleMean.bigDecimalValue(scale, BigDecimal.ROUND_DOWN));
            w.newLine();
            p = Computation.nextPoly(p, a);
        }
    }
}
