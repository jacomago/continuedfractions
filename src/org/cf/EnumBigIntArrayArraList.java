package org.cf;

import java.math.BigInteger;
import java.util.ArrayList;

public class EnumBigIntArrayArraList {
    BigInteger[] currentApproximants;

    private ArrayList<BigInteger> subContinuedFraction;

    EnumBigIntArrayArraList(BigInteger[] cA, ArrayList<BigInteger> cf) {
        this.currentApproximants = cA;
        this.setSubContinuedFraction(cf);
    }

    public ArrayList<BigInteger> getSubContinuedFraction() {
        return subContinuedFraction;
    }

    public void setSubContinuedFraction(ArrayList<BigInteger> subContinuedFraction) {
        this.subContinuedFraction = subContinuedFraction;
    }
}