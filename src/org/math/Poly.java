package org.math;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.analysis.solvers.LaguerreSolver;
import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.fraction.BigFraction;

public class Poly {
    List<BigInteger> coefficients;

    public Poly(List<BigInteger> coeffs) {
        coefficients = coeffs;
    }

    public int getDegree() {
        return coefficients.size() - 1;
    }

    public List<BigInteger> getCoeffs() {
        return coefficients;
    }

    public BigInteger result(BigInteger x) {
        BigInteger y = BigInteger.ZERO;
        int count = 0;
        for (BigInteger coeff : coefficients) {
            // System.out.println("coeff is " + coeff + " count is " + count);
            BigInteger xToCount = x.pow(count);

            // System.out.println("pow is " + xToCount);
            y = y.add(coeff.multiply(xToCount));

            // System.out.println("y is " + y);
            count++;
        }
        return y;
    }

    @Override
    public String toString() {
        String s = "";
        int count = 0;
        for (BigInteger c : coefficients) {

            s = s + " + " + c + "x^" + count;
            count++;
        }
        return s;
    }

    public Poly deriv() {
        ArrayList<BigInteger> newCoeffs = new ArrayList<BigInteger>();
        for (int i = 1; i < coefficients.size(); i++) {
            newCoeffs.add(coefficients.get(i).multiply(BigInteger.valueOf(i)));
        }
        return new Poly(newCoeffs);
    }

    public BigFraction result(BigFraction x) {
        BigInteger p = BigInteger.ZERO;
        BigInteger q = x.getDenominator().pow(this.getDegree());
        int count = 0;
        for (BigInteger coeff : coefficients) {
            // System.out.println("coeff is " + coeff + " count is " + count);
            BigInteger xToCount = x.getNumerator().pow(count)
                    .multiply(x.getDenominator().pow(this.getDegree() - count));

            // System.out.println("pow is " + xToCount);
            p = p.add(xToCount.multiply(coeff));

            // System.out.println("y is " + y);
            count++;
        }
        return new BigFraction(p, q);
    }

    @Override
    public boolean equals(Object other) {

        if (other == null)
            return false;
        if (other == this)
            return true;
        if (!(other instanceof Poly))
            return false;
        Poly p = (Poly) other;
        return (p.coefficients.equals(this.coefficients));
    }

    double[] helperBigIntlistToDoubleArray(List<BigInteger> coefficients) {
        int n = coefficients.size();
        double[] cs = new double[coefficients.size()];
        for (int i = 0; i < n; i++) {
            cs[i] = coefficients.get(i).doubleValue();
        }
        return cs;
    }

    public ArrayList<BigDecimal> solve(int l) {
        ArrayList<BigDecimal> solns = new ArrayList<BigDecimal>();
        MathContext m = new MathContext(l);
        LaguerreSolver lSolve = new LaguerreSolver(Math.pow(10.0, -1 * l));
        double[] coeffs = helperBigIntlistToDoubleArray(coefficients);

        Complex[] fullSolutions = lSolve.solveAllComplex(coeffs, 0.1);
        for (Complex c : fullSolutions) {
            if (Math.abs(c.getImaginary()) < 0.01) {
                solns.add(new BigDecimal(c.getReal(), m));
            }
        }
        return solns;
    }
}
