package org.math;

import java.math.BigInteger;

import org.apache.commons.math3.exception.ZeroException;
import org.apache.commons.math3.fraction.BigFraction;

public class CFFraction extends Number implements Comparable<CFFraction> {
    /**
     * A fraction class that doesn't reduce the fraction for performance.
     */
    private static final long serialVersionUID = -2006871860598636671L;
    private BigInteger num;
    private BigInteger denom;

    public CFFraction(BigInteger num, BigInteger denom) {
        if (denom.compareTo(BigInteger.ZERO) == 0)
            throw new ZeroException();
        if (denom.signum() == -1) {
            denom = denom.negate();
            num = num.negate();
        }
        this.num = num;
        this.denom = denom;
    }

    public CFFraction(BigFraction alpha) {
        this(alpha.getNumerator(), alpha.getDenominator());
    }

    @Override
    public String toString() {
        return this.num + "/" + this.denom;
    }

    BigFraction bigFraction() {
        return new BigFraction(this.num, this.denom);
    }

    @Override
    public double doubleValue() {
        return bigFraction().doubleValue();
    }

    @Override
    public float floatValue() {
        return bigFraction().floatValue();
    }

    @Override
    public int intValue() {
        return bigFraction().intValue();
    }

    @Override
    public long longValue() {
        return bigFraction().longValue();
    }

    public BigInteger getNum() {
        return this.num;
    }

    public BigInteger getDenom() {
        return this.denom;
    }

    @Override
    public int compareTo(CFFraction o) {
        BigInteger temp = this.num.multiply(o.getDenom()).subtract(
                this.denom.multiply(o.getNum()));

        return temp.compareTo(BigInteger.ZERO);
    }

    @Override
    public boolean equals(Object x) {
        // This test is just an optimization, which may or may not help
        if (x == this)
            return true;

        if (!(x instanceof CFFraction))
            return false;

        CFFraction xFrac = (CFFraction) x;

        return bigFraction().equals(xFrac.bigFraction());
    }

    public CFFraction abs() {
        return new CFFraction(this.num.abs(), this.denom);
    }
}
